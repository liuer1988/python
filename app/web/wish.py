from flask import flash, redirect, url_for
from flask_login import current_user, login_required

from app.models.base import db
from app.models.wish import Wish
from . import web


@web.route('/wish/book/<isbn>')
@login_required
def save_to_gifts(isbn):
    if current_user.can_save_to_list(isbn):
        with db.auto_commit():
            wish = Wish()
            wish.uid = current_user.id
            wish.isbn = isbn
            db.session.add(wish)
    else:
        flash("这本书已经添加到赠送清单了")
    return redirect(url_for('web.book_detail', isbn=isbn))
