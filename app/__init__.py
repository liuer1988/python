# 核心对象app的初始化操作
from flask import Flask
from app.models.book import db
from flask_login import LoginManager

login_manager = LoginManager


def create_app():
    app = Flask(__name__)
    app.config.from_object('app.secure')
    app.config.from_object('app.setting')
    register_blueprint(app)

    db.init_app(app)
    login_manager.init_app(app)
    login_manager.login_view = 'web.login'
    login_manager.login_message = '请先登陆或注册'

    db.create_all(app=app)
    return app


# 注册蓝图
def register_blueprint(app):
    from app.web import web
    app.register_blueprint(web)
