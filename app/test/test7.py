class MyResource:

    def query(self):
        print('query data')


from contextlib import contextmanager


@contextmanager
def make_myresource():
    print('connect to resource')
    yield MyResource()
    print('close resource connection')


with make_myresource() as r:
    r.query()

@contextmanager
def book_mark():
    print('<<',end='')
    yield
    print('>>',end='')

with book_mark():
    print('且将生活一饮而尽',end='')