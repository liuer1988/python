import requests


class HTTP:
    # python2 经典类 和 新式类
    # python3 默认是新式类
    @staticmethod
    def get(url, return_json=True):
        r = requests.get(url)
        # restful
        # json

        # 正常流程的一种特例
        if r.status_code != 200:
            return {} if return_json else ''

        return r.json() if return_json else r.text
